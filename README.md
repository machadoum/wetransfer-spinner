# Wetranfer-spinner

Acess https://wetransfer-spinner.herokuapp.com

Or

* Clone the repo
* `npm install`
* `npm start`

Final assets are in the folder `/buid`


## Component

The component is a react component and all files of the component are inside the folder `loader`.

### Hot to use

```JSX
          <Loader
              onStart={() => null}
              onCancel={() => null}
              uploadStatus={{
                remainingTime: 999,
                currentUpload: 0,
                totalUpload: 999,
                recipients: 0,
                files: 0,
              }} />
```

### Props
* onCancel - function called when cancel is clicked
* onStart - function called when start is clicked
* uploadStatus - object with the upload status
	* files - number of files
	* recipients - number of recipients
	* totalUpload - total upload size in bytes
	* currentUpload - current uploaded size in bytes
	* remainingTime - estimated time is miliseconds


### PropTypes

```javascript
  {
    onCancel: PropTypes.func.isRequired,
    onStart: PropTypes.func.isRequired,
    uploadStatus: PropTypes.shape({
      files: PropTypes.number.isRequired,
      recipients: PropTypes.number.isRequired,
      totalUpload: PropTypes.number.isRequired,
      currentUpload: PropTypes.number.isRequired,
      remainingTime: PropTypes.number.isRequired
  }
```

### Example

The files `App.js` is an example of how to use the component. This file simulates an upload process and updates the component.


## Decisions
I consider that who uses this component must implement an upload process and would like to provide all upload related data to the component. This way the component can be responsible for all data visualization while another layer would be responsible for the upload process.

Most decisions were made considering time as the most critical constraint since I only have one weekend to do everything. That's why I choose `create-react-app`, allowing me to fast prototype.

All component is cover by unit tests. Unfortenly the time constraint didn't allow me to do any other kind of test. I also didn't use any CSS preprocessor for the same reason.





