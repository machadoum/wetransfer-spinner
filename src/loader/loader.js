import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import './loader.css'
import Spinner from './spinner'
import { CancelButton, StartButton } from './button'
import filesize from 'file-size'
import humanizeDuration from 'humanize-duration'

export default class Loader extends Component {
  state = { loading: false }
  static propTypes = {
    onCancel: PropTypes.func.isRequired,
    onStart: PropTypes.func.isRequired,
    uploadStatus: PropTypes.shape({
      files: PropTypes.number.isRequired,
      recipients: PropTypes.number.isRequired,
      totalUpload: PropTypes.number.isRequired,
      currentUpload: PropTypes.number.isRequired,
      remainingTime: PropTypes.number.isRequired
    })
  }

  render() {
    return (
      <div className='loader'>
        <div className='loader-main'>
          {this.state.loading ?
            <Fragment>
              <Spinner percentual={this.props.uploadStatus.currentUpload / this.props.uploadStatus.totalUpload} />
              <div className='loader-content'>
                <h2 className='loader__title'>Transferring...</h2>
                <LoaderDescription {...this.props.uploadStatus} />
              </div>
            </Fragment> :
            <StartTranfer {...this.props.uploadStatus} />}
        </div>
        <div className='loader-footer'>
          { this.state.loading ?
            <CancelButton onClick={this.onCancel} /> :
            <StartButton onClick={this.onStart} /> }
        </div>
      </div>
    )
  }

  onStart = () => {
    this.props.onStart();
    this.setState({ loading: true })
  }

  onCancel = () => {
    this.props.onCancel();
    this.setState({ loading: false })
  }
}

const humanizeSize = (size) => filesize(size, { fixed: 0 }).human('si')

const LoaderDescription = ({ files, recipients, totalUpload, currentUpload, remainingTime }) =>
  <Fragment>
    <div className='loader__text'>Sending <a href='#'>{files} files to {recipients} recipients</a></div>
    <div className='loader__text'>{humanizeSize(currentUpload)} of {humanizeSize(totalUpload)} uploaded</div>
    <div className='loader__text'>{humanizeDuration(remainingTime, { round: true })} remaining</div>
  </Fragment>

const StartTranfer = ({ files, totalUpload }) =>
  <Fragment>
    <h2>Let's tranfer!</h2>
    <p>{`${files} files - ${humanizeSize(totalUpload)} remaining`}</p>
  </Fragment>
