import React from 'react'
import { mount } from 'enzyme'
import Spinner, { CIRC, ROUNDING_LINECAP, Circle } from './spinner'

describe("Spinner value", () => {
  it('renders value 20% when percentual is 0.2', () => {
    const wrapper = mount(<Spinner percentual={0.2}/>)
    expect(wrapper.find('.spinner__value').text()).toEqual('20%')
  })

  it('renders value 0% when percentual is 0', () => {
    const wrapper = mount(<Spinner percentual={0} />)
    expect(wrapper.find('.spinner__value').text()).toEqual('0%')
  })

  it('renders value 100% when percentual is 1', () => {
    const wrapper = mount(<Spinner percentual={1} />)
    expect(wrapper.find('.spinner__value').text()).toEqual('100%')
  })
})

describe("Spinner circle", () => {
  it('renders two circles', () => {
    const wrapper = mount(<Spinner />)
    expect(wrapper.find('circle')).toHaveLength(2)
  })

  it('renders percentual circ length equal to circumference', () => {
    const wrapper = mount(<Spinner />)

    const percentualCirc = wrapper.find('circle').last()

    expect(percentualCirc.props().style.strokeDasharray).toEqual(CIRC)
  })

  it('renders percentual 0 when no parameters given', () => {
    const wrapper = mount(<Spinner />)

    const percentualCirc = wrapper.find('circle').last()

    expect(percentualCirc.props().style.strokeDashoffset).toEqual(CIRC + ROUNDING_LINECAP)
  })

  it('renders half circle when percentual is 50%', () => {
    const wrapper = mount(<Spinner percentual={0.5} />)

    const percentualCirc = wrapper.find('circle').last()

    expect(percentualCirc.props().style.strokeDashoffset).toEqual(CIRC / 2 + ROUNDING_LINECAP)
  })

  it('renders empty circle when percentual is 100%', () => {
    const wrapper = mount(<Spinner percentual={1} />)

    const percentualCirc = wrapper.find('circle').last()

    expect(percentualCirc.props().style.strokeDashoffset).toEqual(0 + ROUNDING_LINECAP)
  })

  it('renders apropiriate lenght when percentual is 20%', () => {
    const wrapper = mount(<Spinner percentual={0.2} />)

    const percentualCirc = wrapper.find('circle').last()

    expect(percentualCirc.props().style.strokeDashoffset).toEqual(0.8 * CIRC + ROUNDING_LINECAP)
  })
})
