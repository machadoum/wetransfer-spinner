import React from 'react'
import { shallow } from 'enzyme'
import { StartButton, CancelButton } from './button'

it('renders StartButton as a primary button', () => {
  const wrapper = shallow(<StartButton />).dive()
  expect(wrapper.props().className).toContain('button--primary')
})

it('renders StartButton with text Start', () => {
  const wrapper = shallow(<StartButton />).dive()
  expect(wrapper.text()).toContain('Start')
})

it('renders Cancel as a secondary button', () => {
  const wrapper = shallow(<CancelButton />).dive()
  expect(wrapper.props().className).toContain('button--secondary')
})

it('renders Cancelbutton with text cancel', () => {
  const wrapper = shallow(<CancelButton />).dive()
  expect(wrapper.text()).toContain('Cancel')
})