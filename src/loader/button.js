import React from 'react'
import './button.css'

const Button = ({ text, onClick, primary = false }) =>
  <button className={primary ? 'button button--primary' : 'button button--secondary'} onClick={onClick}>
    {text}
  </button>

export const CancelButton = (props) => <Button {...props} text='Cancel'/>
export const StartButton = (props) => <Button {...props} primary text='Start'/>

