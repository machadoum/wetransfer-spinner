import React from 'react'
import './spinner.css'

const RADIO = 80
export const CIRC = 2 * Math.PI * RADIO
export const ROUNDING_LINECAP = 2.5 // It's required to compensate strokeLinecap. Otherwise, 99% would look like 100%

export default ({ percentual = 0 }) =>
  <div className='spinner'>
    <span className='spinner__value'>
      {Math.floor(percentual * 100)}
      <span className='spinner__perc'>%</span>
    </span>
    <svg height="170" width="170" className="spinner__circle" shapeRendering="geometricPrecision" viewBox="0 0 170 170">
      <Circle color='rgb(232, 235, 237)' />

      <Circle color={'rgb(64, 159, 255)'}
        style={{
          strokeDashoffset: (1 - percentual) * CIRC + ROUNDING_LINECAP,
          strokeDasharray: CIRC
        }} />
    </svg>
  </div>

export const Circle = ({ style, color }) =>
  <circle
    cx="85" cy="85" r={RADIO}
    strokeLinecap="round"
    fill="transparent"
    style={{
      strokeWidth: 10,
      stroke: color,
      ...style
    }} />