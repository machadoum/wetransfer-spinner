import React from 'react'
import { shallow, mount } from 'enzyme'
import Loader from './loader'
import { StartButton, CancelButton } from './button'
import Spinner from './spinner'

const defaultUploadStatus = {
  remainingTime: 1, currentUpload: 1, totalUpload: 1, recipients: 1, files: 1
}

const defaultProps = {
  onStart: () => null,
  onCancel: () => null,
  uploadStatus: defaultUploadStatus
}

describe('when it is not loading', () => {
  it('renders amount of files', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{...defaultUploadStatus, files: 2}} />)
    expect(wrapper.html()).toContain('2 files')
  })

  it('renders files size', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{ ...defaultUploadStatus, totalUpload: 1024000 }} />)
    expect(wrapper.html()).toContain('1 MB remaining')
  })

  it('renders start button', () => {
    const wrapper = shallow(<Loader {...defaultProps} />)
    expect(wrapper.find(StartButton)).toBeDefined()
  })

  it('calls onStart prop when started is clicked', () => {
    const onStart = jest.fn()
    const wrapper = shallow(<Loader {...defaultProps} onStart={onStart}/>)
    wrapper.find(StartButton).simulate('click')
    expect(onStart).toHaveBeenCalled()
  })
})

describe('when it is loading', () => {
  it('renders amount of files', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{ ...defaultUploadStatus, recipients: 9 }} />)
    wrapper.find(StartButton).simulate('click')
    expect(wrapper.html()).toContain('9 recipients')
  })

  it('renders amount of files', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{ ...defaultUploadStatus, files: 5 }} />)
    wrapper.find(StartButton).simulate('click')
    expect(wrapper.html()).toContain('5 files')
  })


  it('renders remaining time', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{ ...defaultUploadStatus, remainingTime: 10000 }} />)
    wrapper.find(StartButton).simulate('click')
    expect(wrapper.html()).toContain('10 seconds')
  })

  it('renders files size', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{ ...defaultUploadStatus, totalUpload: 1024000 }} />)
    wrapper.find(StartButton).simulate('click')
    expect(wrapper.html()).toContain('1 MB uploaded')
  })

  it('renders current upload size', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{ ...defaultUploadStatus, currentUpload: 1024000 }} />)
    wrapper.find(StartButton).simulate('click')
    expect(wrapper.html()).toContain('1 MB of')
  })

  it('renders progress', () => {
    const wrapper = shallow(<Loader {...defaultProps} uploadStatus={{ ...defaultUploadStatus, currentUpload: 2, totalUpload: 10 }} />)
    wrapper.find(StartButton).simulate('click')
    expect(wrapper.find(Spinner).props().percentual).toBe(0.2)
  })

  it('calls onCancel prop when cancel is clicked', () => {
    const onCancel = jest.fn()
    const wrapper = mount(<Loader {...defaultProps} onStart={onCancel} />)
    wrapper.find(StartButton).simulate('click')
    wrapper.find(CancelButton).simulate('click')
    expect(onCancel).toHaveBeenCalled()
  })
})


