import React, { Component } from 'react'
import './App.css'
import logo from './logo.svg'
import Loader from './loader/loader'
import celebrateGif from './celebrate.gif'

const TOTAL_UPLOAD_TIME = 30000
const TOTAL_SIZE = 1e+8

export default class App extends Component {
  state = {
    currentUpload: 0,
    timeoutRef: null
  }

  render() {
    const remainingTime = Math.round((1 - this.state.currentUpload / TOTAL_SIZE) * TOTAL_UPLOAD_TIME)

    return (
      <div className='App'>
        <img className='App-logo' src={logo} alt='logo' />
        <div className='App-box'>
          {this.state.currentUpload < TOTAL_SIZE ?
            <Loader
              onStart={this.onStart}
              onCancel={this.onCancel}
              uploadStatus={{
                remainingTime: remainingTime,
                currentUpload: this.state.currentUpload,
                totalUpload: TOTAL_SIZE,
                recipients: 2,
                files: 3,
              }} /> :
          <div className='App-finished'>
            <img className='App-celebrate' src={celebrateGif} alt='celebration' />
            <h2 className='App-done'>You’re done!</h2>
          </div>}
        </div>
      </div>
    )
  }

  onCancel = () => {
    clearTimeout(this.state.timeoutRef)
    this.setState({
      currentUpload: 0,
      timeoutRef: null
    })
  }

  onStart = () => {
    this.setState({
      timeoutRef: setTimeout(() => this.fakeUploadProcess(), 500)
    })
  }

  fakeUploadProcess() {
    if (this.state.currentUpload !== TOTAL_SIZE) {
      this.setState({
        currentUpload: this.state.currentUpload + TOTAL_SIZE / 100,
        timeoutRef: setTimeout(() => this.fakeUploadProcess(), TOTAL_UPLOAD_TIME / 100)
      })
    }
  }
}